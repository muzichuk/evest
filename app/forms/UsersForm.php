<?php
namespace EVEST\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use EVEST\Models\Roles;

class UsersForm extends Form
{

    public function initialize($entity = null, $options = null)
    {

        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id');
        }

        $this->add($id);

        $first_name = new Text('first_name', [
            'placeholder' => 'First Name',
            'class' => 'form-control'
        ]);

        $first_name->addValidators([
            new PresenceOf([
                'message' => 'First Name is required'
            ])
        ]);

        $this->add($first_name);

        $last_name = new Text('last_name', [
            'placeholder' => 'Last Name',
            'class' => 'form-control'
        ]);

        $last_name->addValidators([
            new PresenceOf([
                'message' => 'Last Name is required'
            ])
        ]);

        $this->add($last_name);
        
        $email = new Text('email', [
            'placeholder' => 'Email',
            'class' => 'form-control'
        ]);

        $email->addValidators([
            new PresenceOf([
                'message' => 'The e-mail is required'
            ]),
            new Email([
                'message' => 'The e-mail is not valid'
            ])
        ]);

        $this->add($email);

        $roles = Roles::find([
            'active = :active:',
            'bind' => [
                'active' => 'Y'
            ]
        ]);

        $this->add(new Select('role_id', $roles, [
            'using' => [
                'id',
                'name'
            ],
            'useEmpty' => true,
            'emptyText' => '...',
            'emptyValue' => 4, // Investor Role
            'class' => 'form-control'
        ]));

        $this->add(new Select('active', [
            'Y' => 'Yes',
            'N' => 'No'
        ], [
            'class' => 'form-control'
        ]));
    }
}

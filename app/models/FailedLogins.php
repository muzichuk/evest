<?php
namespace EVEST\Models;

use EVEST\Models\Tenant_Base;

/**
 * FailedLogins
 * This model registers unsuccessfull logins registered and non-registered users have made
 */
class FailedLogins extends Tenant_Base
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $ip_address;

    /**
     *
     * @var string
     */
    public $user_agent;

    /**
     *
     * @var integer
     */
    public $attempted;

    /**
     *
     * @var string
     */
    public $created_at;


    public function initialize()
    {
        $this->skipAttributes(['created_at']);
        
        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', [
            'alias' => 'user'
        ]);
    }
    

}

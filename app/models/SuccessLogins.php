<?php
namespace EVEST\Models;

use EVEST\Models\Tenant_Base;

/**
 * SuccessLogins
 * This model registers successfull logins registered users have made
 */
class SuccessLogins extends Tenant_Base
{
    
    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $ip_address;

    /**
     *
     * @var string
     */
    public $user_agent;

    /**
     *
     * @var string
     */
    public $client;

    /**
     *
     * @var string
     */
    public $os;

    public function initialize()
    {
        $this->skipAttributes(['created_at']);
        
        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', [
            'alias' => 'user'
        ]);
        
        $this->belongsTo('tid', __NAMESPACE__ . '\Tenants', 'id', [
            'alias' => 'tenant'
        ]);
    }
}

<?php
namespace EVEST\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

/**
 * EVEST\Models\Tenants
 * 
 */
class Tenants extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $domain;

    /**
     *
     * @var string
     */
    public $active;

    
    public function initialize()
    {

        $this->skipAttributes(['created_at', 'modified_at']);
        
        $this->hasMany('id', __NAMESPACE__ . '\Users', 'tid', [
            'alias' => 'users',
            'foreignKey' => [
                'message' => 'Tenant cannot be deleted because it has Users in the system'
            ]
        ]);

    }
}

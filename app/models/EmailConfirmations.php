<?php
namespace EVEST\Models;

use EVEST\Models\Tenant_Base;

/**
 * EmailConfirmations
 * Stores the reset password codes and their evolution
 */
class EmailConfirmations extends Tenant_Base
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     * 
     * @var string
     */
    public $code;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $modified_at;

    /**
     * 
     * @var string
     */
    public $confirmed;


    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        // Generate a random confirmation code
        $this->code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));

        // Set status to non-confirmed
        $this->confirmed = 'N';
        
        return parent::beforeValidationOnCreate();
        
    }

    /**
     * Send a confirmation e-mail to the user after create the account
     */
    public function afterCreate()
    {
        //$this->getDI()
        //    ->getMail()
        //    ->send([
        //        $this->user->email => $this->user->name
        //    ], "Please confirm your email", 'confirmation', [
        //        'confirmUrl' => '/confirm/' . $this->code . '/' . $this->user->email
        //    ]);
    }

    public function initialize()
    {
        $this->skipAttributes(['created_at', 'modified_at']);
        
        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', [
            'alias' => 'user'
        ]);
    }
    
}

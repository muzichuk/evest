<?php
namespace EVEST\Models;

use Phalcon\Mvc\Model;

/**
 * Permissions
 * Stores the permissions by role
 */
class Permissions extends Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $role_id;

    /**
     *
     * @var string
     */
    public $resource;

    /**
     *
     * @var string
     */
    public $action;

    /**
     *
     * @var string
     */
    public $created_at;

    public function initialize()
    {
        $this->skipAttributes(['created_at']);
        
        $this->belongsTo('role_id', __NAMESPACE__ . '\Roles', 'id', [
            'alias' => 'role'
        ]);
    }
    
    public function afterSave()
    {
        $this->getDI()->get('acl')->rebuild();
    }
    
    public function afterDelete()
    {
        $this->getDI()->get('acl')->rebuild();
    }
}

<?php
namespace EVEST\Models;

use Phalcon\DI;
use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

/**
 * EVEST\Models\Tenant_Base
 * 
 */
class Tenant_Base extends Model
{

    /**
     *
     * @var integer
     */
    protected $tid;
    
    
    /**
     * Find only Tenant records
     */
    public static function findFirst( $criteria = [] )
    {
        if( isset( $criteria['conditions'] ) ) {
            $criteria['conditions'] .= ' AND tid = :tid:';
        } else {
            $criteria['conditions'] = 'tid = :tid:';
        }

        $criteria['bind']['tid'] = \Phalcon\DI::getDefault()->getSession()->get('tid');

        return parent::findFirst( $criteria );
    }

    /**
     * Find only Tenant records
     */
    public static function find( $criteria = [] )
    {
        if( isset( $criteria['conditions'] ) ) {
            $criteria['conditions'] .= ' AND tid = :tid:';
        } else {
            
            if( isset($criteria['di']) && isset($criteria[0]) ) {
                $criteria[0] .= ' AND [tid] = :tid:';
            } else {
                $criteria['conditions'] = 'tid = :tid:';
            }
        }

        $criteria['bind']['tid'] = \Phalcon\DI::getDefault()->getSession()->get('tid');

        return parent::find( $criteria );
    }

    /**
     * Set Tenant ID on Create
     */
    public function beforeValidationOnCreate()
    {
        $this->tid = $this->getDI()->getSession()->get('tid');
        
    }
    
    /**
     * Verify Tenant ID before delete
     */
    public function beforeDelete()
    {
        $tid = $this->getDI()->getSession()->get('tid');
        
        if( $this->tid !== $tid ) {
            $this->getDI()
                ->getFlash()
                ->error( "TID Error! This TID=$this->tid. Sess TID=$tid" );
            return false;
        }
        
    }

}

<?php
namespace EVEST\Models;

use EVEST\Models\Tenant_Base;
use Mailgun\Mailgun;

/**
 * ResetPasswords
 * Stores the reset password codes and their evolution
 */
class ResetPasswords extends Tenant_Base
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $code;

    /**
     *
     * @var string
     */
    public $created_at;

    /**
     *
     * @var string
     */
    public $modified_at;

    /**
     *
     * @var string
     */
    public $reset;

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {
        // Generate a random confirmation code
        $this->code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));

        // Set status to non-confirmed
        $this->reset = 'N';
        
        return parent::beforeValidationOnCreate();
    }

    /**
     * Send an e-mail to users allowing him/her to reset his/her password
     */
    public function afterCreate()
    {
        //$this->getDI()
        //    ->getMail()
        //    ->send([
        //        $this->user->email => $this->user->name
        //    ], "Reset your password", 'reset', [
        //        'resetUrl' => '/reset-password/' . $this->code . '/' . $this->user->email
        //    ]);
        $mg = Mailgun::create('key-a6a689856947c7cdd44c647f05f10652');
        
        # Now, compose and send your message.
        # $mg->messages()->send($domain, $params);
        $mg->messages()->send('mg.evesttech.com', [
          'from'    => 'vmuzichuk@mg.evesttech.com',
          'to'      => 'vmuzichuk@realtyevest.com',
          'subject' => 'test1',
          'html'    => '<h1>test1</h1>'
        ]);
    }

    public function initialize()
    {
        $this->skipAttributes(['created_at', 'modified_at']);
        
        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', [
            'alias' => 'user'
        ]);
    }
}

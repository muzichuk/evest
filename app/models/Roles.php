<?php
namespace EVEST\Models;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Relation;

/**
 * EVEST\Models\Roles
 * All the role levels in the application. Used in conjenction with ACL lists
 */
class Roles extends Model
{

    /**
     * ID
     * @var integer
     */
    public $id;

    /**
     * Name
     * @var string
     */
    public $name;

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
        $this->skipAttributes(['created_at', 'modified_at']);
        
        $this->hasMany('id', __NAMESPACE__ . '\Users', 'role_id', [
            'alias' => 'users',
            'foreignKey' => [
                'message' => 'Role cannot be deleted because it\'s used on Users'
            ]
        ]);

        $this->hasMany('id', __NAMESPACE__ . '\Permissions', 'role_id', [
            'alias' => 'permissions',
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE
            ]
        ]);
    }
    
    public function afterSave()
    {
        $this->getDI()->get('acl')->rebuild();
    }
    
    public function afterDelete()
    {
        $this->getDI()->get('acl')->rebuild();
    }
}

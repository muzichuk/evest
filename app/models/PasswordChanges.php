<?php
namespace EVEST\Models;

use EVEST\Models\Tenant_Base;

/**
 * PasswordChanges
 * Register when a user changes his/her password
 */
class PasswordChanges extends Tenant_Base
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $ip_address;

    /**
     *
     * @var string
     */
    public $user_agent;

    /**
     *
     * @var string
     */
    public $os;

    /**
     *
     * @var string
     */
    public $client;

    /**
     *
     * @var string
     */
    public $location;

    /**
     *
     * @var string
     */
    public $created_at;
    

    public function initialize()
    {
        $this->skipAttributes(['created_at']);
        
        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', [
            'alias' => 'user'
        ]);
    }
    
}

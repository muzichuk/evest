<?php
namespace EVEST\Models;

use EVEST\Models\Tenant_Base;

/**
 * RememberTokens
 * Stores the remember me tokens
 */
class RememberTokens extends Tenant_Base
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $token;

    /**
     *
     * @var string
     */
    public $user_agent;

    /**
     *
     * @var integer
     */
    public $created_at;

    /**
     * 
     */
    public function beforeValidationOnCreate()
    {
        parent::beforeValidationOnCreate();
        // Timestamp the confirmaton
        $this->created_at = time();
    }

    public function initialize()
    {
        $this->belongsTo('user_id', __NAMESPACE__ . '\Users', 'id', [
            'alias' => 'user'
        ]);
    }
    
}

{{ content() }}

<form method="post" autocomplete="off" action="{{ url("users/changePassword") }}">

    <div class="text-center col-12 row">

        <h2>Change Password</h2>

        <div class="col-12 row my-3">
            <div class="form-group col-12 col-sm-6">
                {{ form.render("password") }}
            </div>
    
            <div class="form-group col-12 col-sm-6">
                {{ form.render("confirmPassword") }}
            </div>
    
        </div>

    </div>
    
    <div class="form-group col-12 float-left">
        {{ submit_button("Change Password", "class": "btn btn-primary") }}
    </div>

</form>
{{ content() }}


<div class="col-12 ">

<div class="col-12">
<!--    <div class="previous pull-left d-inline">
        {{ link_to("users/index", "Back", "class": "btn btn-secondary") }}
    </div>-->
    <div class="pull-right d-inline mb-2">
        {{ link_to("users/create", "Create User", "class": "btn btn-primary") }}
    </div>
</div>

{% for user in page.items %}
{% if loop.first %}
<table class="table table-sm table col-12">
    <thead class="thead-default">
        <tr>
            <th class="hidden-sm-down">Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Active</th>
            <th></th>
            <th class="hidden-sm-down"></th>
        </tr>
    </thead>
    <tbody>
{% endif %}
        <tr scope="row" class="{{ user.active == 'Y' ? '' : 'table-warning' }}">
            <td class="hidden-sm-down">{{ user.id }}</td>
            <td>{{ user.name }}</td>
            <td>{{ user.email }}</td>
            <td>{{ user.role.name }}</td>
            <td>{{ user.active == 'Y' ? 'Yes' : 'No' }}</td>
            <td>{{ link_to("users/edit/" ~ user.id, 'Edit', "class": "btn btn-warning") }}</td>
            <td class="hidden-sm-down">{{ link_to("users/delete/" ~ user.id, 'Delete', "class": "btn btn-danger") }}</td>
        </tr>
{% if loop.last %}
    </tbody>
</table>

<nav aria-label="...">
  <ul class="pagination">
    <li class="page-item ">
        {{ link_to("users/search", 'First', "class": "page-link", "tabindex": "-1") }}
    </li>
    <li class="page-item">
      {{ link_to("users/search?page=" ~ page.before, 'Previous', "class": "page-link", "tabindex": "-1") }}
    </li>
    <li class="page-item">
      {{ link_to("users/search?page=" ~ page.next, 'Next', "class": "page-link", "tabindex": "-1") }}
    </li>
    <li class="page-item">
      {{ link_to("users/search?page=" ~ page.last, 'Last', "class": "page-link", "tabindex": "-1") }}
    </li>
    <span class="ml-4 mt-2">{{ page.current }} / {{ page.total_pages }}</span>
  </ul>
</nav>



{% endif %}
{% else %}
    No users are recorded
{% endfor %}
</div>

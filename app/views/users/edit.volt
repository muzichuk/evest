
<form method="post" autocomplete="off">


<div class="col-12 mb-5">
    <div class="previous pull-left d-inline">
        {{ link_to("users/index", "Back", "class": "btn btn-secondary") }}
    </div>
    <div class="pull-right d-inline">
        {{ submit_button("Save", "class": "btn btn-big btn-success") }}
    </div>
    <div class="text-center d-inline">
        <h2>Edit users</h2>
    </div>
</div>

{{ content() }}

<div class="col-12 text-center">

    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" href="#A" data-toggle="tab">Basic</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#B" data-toggle="tab">Successful Logins</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#C" data-toggle="tab">Password Changes</a>
        </li>
    </ul>

<div class="tabbable">
    <div class="tab-content">
        <div class="tab-pane active" id="A">

            {{ form.render("id") }}

            <div class="col-12 row mt-2">

                <div class="form-group col-6">
                    <label for="first_name">First Name:</label>
                    {{ form.render("first_name") }}
                </div>

                <div class="form-group col-6">
                    <label for="last_name">Last Name:</label>
                    {{ form.render("last_name") }}
                </div>
                
                <div class="form-group col-6">
                    <label for="email">Email:</label>
                    {{ form.render("email") }}
                </div>

                <div class="form-group col-6">
                    <label for="role_id">Role</label>
                    {{ form.render("role_id") }}
                </div>


            </div>

            <div class="col-12 row">

                <div class="form-group col-4">
                    <label for="active">Confirmed:</label>
                    {{ form.render("active") }}
                </div>


            </div>
        </div>

        <div class="tab-pane" id="B">
            <p>
                <table class="table table-bordered table-striped" align="center">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th>Device</th>
                            <th>Client</th>
                            <th>Location</th>
                        </tr>
                    </thead>
                    <tbody>
                    {% for login in user.successLogins %}
                        <tr>
                            <td>{{ login.created_at }}</td>
                            <td>{{ login.os }}</td>
                            <td>{{ login.client }}</td>
                            <td>{{ login.location }}</td>
                        </tr>
                    {% else %}
                        <tr><td colspan="3" align="center">User does not have successfull logins</td></tr>
                    {% endfor %}
                    </tbody>
                </table>
            </p>
        </div>

        <div class="tab-pane" id="C">
            <p>
                <table class="table table-bordered table-striped" align="center">
                    <thead>
                        <tr>
                            <th>Time</th>
                            <th>Device</th>
                            <th>Client</th>
                            <th>Location</th>
                        </tr>
                    </thead>
                    <tbody>
                    {% for change in user.passwordChanges %}
                        <tr>
                            <td>{{ change.created_at }}</td>
                            <td>{{ change.os }}</td>
                            <td>{{ change.client }}</td>
                            <td>{{ change.location }}</td>
                        </tr>
                    {% else %}
                        <tr><td colspan="3" align="center">User has not changed his/her password</td></tr>
                    {% endfor %}
                    </tbody>
                </table>
            </p>
        </div>

    </div>
</div>

    </form>
</div>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>{{ tenant_name }}</title>
	    <link rel="icon" href="{{ favicon }}">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet">
		<link href="{{ tenant_css }}" rel="stylesheet">
	</head>
	<body>

		{{ content() }}

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
		{{ javascript_include("js/custom.js") }}
		
		{% if logged_in %}
		
		{% endif %}
	</body>
</html>
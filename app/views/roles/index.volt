{{ content() }}

<div align="right">
    {{ link_to("roles/create", "<i class='icon-plus-sign'></i> Create Roles", "class": "btn btn-primary") }}
</div>

<form method="post" action="{{ url("roles/search") }}" autocomplete="off">

    <div class="center scaffold">

        <h2>Search roles</h2>

        <div class="clearfix">
            <label for="id">Id</label>
            {{ form.render("id") }}
        </div>

        <div class="clearfix">
            <label for="name">Name</label>
            {{ form.render("name") }}
        </div>

        <div class="clearfix">
            {{ submit_button("Search", "class": "btn btn-primary") }}
        </div>

    </div>

</form>
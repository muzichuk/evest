{{ content() }}

<div align="center" class="well">

	{{ form('class': 'form-search') }}

	<div align="left">
		<h2>Log In</h2>
	</div>
		<div class="form-group">
			{{ form.render('email') }}
		</div>
		<div class="form-group">
		{{ form.render('password') }}
		</div>
		<div class="form-group">
		{{ form.render('Login') }}
		</div>

		<div align="center" class="remember">
			{{ form.render('remember') }}
			{{ form.label('remember') }}
		</div>

		{{ form.render('csrf', ['value': security.getToken()]) }}

		<hr>

		<div class="forgot">
			{{ link_to("session/forgotPassword", "Forgot my password") }}
		</div>

		<div class="forgot">
			{{ link_to("session/signup", "Create Account") }}
		</div>

	</form>

</div>
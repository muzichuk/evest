{{ content() }}

<div align="center">

	{{ form('class': 'form-search') }}

		<div align="left">
			<h2>Sign Up</h2>
		</div>

		<div class="signup">
			<div class="form-group">
				{{ form.render('first_name') }}
				{{ form.messages('first_name') }}
			</div>
			<div class="form-group">
				{{ form.render('last_name') }}
				{{ form.messages('last_name') }}
			</div>
			<div class="form-group">
				{{ form.render('email') }}
				{{ form.messages('email') }}
			</div>
			<div class="form-group">
				{{ form.render('password') }}
				{{ form.messages('password') }}
			</div>
			<div class="form-group">
				{{ form.render('confirmPassword') }}
				{{ form.messages('confirmPassword') }}
			</div>
			<div class="form-group">
				<div align="right"></div>
				<div>{{ form.render('Sign Up') }}</div>
			</div>
		</div>

		{{ form.render('csrf', ['value': security.getToken()]) }}
		{{ form.messages('csrf') }}

		<hr>

	</form>

</div>
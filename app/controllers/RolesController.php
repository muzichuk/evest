<?php
namespace EVEST\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use EVEST\Forms\RolesForm;
use EVEST\Models\Roles;

/**
 * EVEST\Controllers\RolesController
 * CRUD to manage roles
 */
class RolesController extends ControllerBase
{

    /**
     * Default action. Set the private (authenticated) layout (layouts/private.volt)
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('private');
    }

    /**
     * Default action, shows the search form
     */
    public function indexAction()
    {
        $this->persistent->conditions = null;
        $this->view->form = new RolesForm();
    }

    /**
     * Searches for roles
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'EVEST\Models\Roles', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $roles = Roles::find($parameters);
        if (count($roles) == 0) {

            $this->flash->notice("The search did not find any roles");

            return $this->dispatcher->forward([
                "action" => "index"
            ]);
        }

        $paginator = new Paginator([
            "data" => $roles,
            "limit" => 10,
            "page" => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Creates a new Role
     */
    public function createAction()
    {
        if ($this->request->isPost()) {

            $role = new Roles([
                'name' => $this->request->getPost('name', 'striptags'),
                'active' => $this->request->getPost('active')
            ]);

            if (!$role->save()) {
                $this->flash->error($role->getMessages());
            } else {
                $this->flash->success("Role was created successfully");
            }

            Tag::resetInput();
        }

        $this->view->form = new RolesForm(null);
    }

    /**
     * Edits an existing Role
     *
     * @param int $id
     */
    public function editAction($id)
    {
        $role = Roles::findFirstById($id);
        if (!$role) {
            $this->flash->error("Role was not found");
            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if ($this->request->isPost()) {

            $role->assign([
                'name' => $this->request->getPost('name', 'striptags'),
                'active' => $this->request->getPost('active')
            ]);

            if (!$role->save()) {
                $this->flash->error($role->getMessages());
            } else {
                $this->flash->success("Role was updated successfully");
            }

            Tag::resetInput();
        }

        $this->view->form = new RolesForm($role, [
            'edit' => true
        ]);

        $this->view->role = $role;
    }

    /**
     * Deletes a Role
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $role = Roles::findFirstById($id);
        if (!$role) {

            $this->flash->error("Role was not found");

            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if (!$role->delete()) {
            $this->flash->error($role->getMessages());
        } else {
            $this->flash->success("Role was deleted");
        }

        return $this->dispatcher->forward([
            'action' => 'index'
        ]);
    }
}

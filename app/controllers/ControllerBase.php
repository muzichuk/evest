<?php
namespace EVEST\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use EVEST\Models\Tenants;

/**
 * ControllerBase
 * This is the base controller for all controllers in the application
 *
 * @property \EVEST\Auth\Auth auth
 */
class ControllerBase extends Controller
{
    /**
     * Execute before the router so we can determine if this is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    
    
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();

        // Only check permissions on private controllers
        if ($this->acl->isPrivate($controllerName)) {

            // Get the current identity
            $identity = $this->auth->getIdentity();
            $this->view->setVar('logged_in', is_array($identity));

            // If there is no identity available the user is redirected to index/index
            if (!is_array($identity)) {

                $this->flash->notice('You must Log In');

                $dispatcher->forward([
                    'controller' => 'session',
                    'action' => 'login'
                ]);
                return false;
            }

            // Check if the user have permission to the current option
            $actionName = $dispatcher->getActionName();
            if (!$this->acl->isAllowed($identity['role'], $controllerName, $actionName)) {

                $this->flash->notice('You don\'t have access to this page: ' . $controllerName . ':' . $actionName);

                if ($this->acl->isAllowed($identity['role'], $controllerName, 'index')) {
                    
                    $dispatcher->forward([
                        'controller' => $controllerName,
                        'action' => 'index'
                    ]);
                    
                } else {
                    
                    $dispatcher->forward([
                        'controller' => 'user_control',
                        'action' => 'index'
                    ]);
                    
                }

                return false;
            }
        }
        
        
        /*
         *
         *  SET Tenant Styles
         *
         */
        $session = $this->getDI()->get('session');
        
        $this->view->tid           = $session->get('tid');
        $this->view->tenant_name   = $session->get('tenant_name');
        
        $favicon                   = '/img/favicon/' . $this->view->tid . '.png';
        // default to evest favicon
        $this->view->favicon       = file_exists(BASE_PATH . '/public' . $favicon) ? $favicon : '/img/favicon/evest.png';
        
        $primary_color   = $session->get('primary_color');
        $secondary_color = $session->get('secondary_color');
        
        $this->view->tenant_css = "/css/tenant.css.php?tid=" . $this->view->tid;//?primary=$primary_color&secondary=$secondary_color";
        
        
    }
    
}

<?php
namespace EVEST\Controllers;

use EVEST\Models\Roles;
use EVEST\Models\Permissions;

/**
 * View and define permissions for the various role levels.
 */
class PermissionsController extends ControllerBase
{

    /**
     * View the permissions for a role level, and change them if we have a POST.
     */
    public function indexAction()
    {
        $this->view->setTemplateBefore('private');

        if ($this->request->isPost()) {

            // Validate the role
            $role = Roles::findFirstById($this->request->getPost('roleId'));

            if ($role) {

                if ($this->request->hasPost('permissions') && $this->request->hasPost('submit')) {

                    // Deletes the current permissions
                    $role->getPermissions()->delete();

                    // Save the new permissions
                    foreach ($this->request->getPost('permissions') as $permission) {

                        $parts = explode('.', $permission);

                        $permission = new Permissions();
                        $permission->rolesId = $role->id;
                        $permission->resource = $parts[0];
                        $permission->action = $parts[1];

                        $permission->save();
                    }

                    $this->flash->success('Permissions were updated with success');
                }

                // Rebuild the ACL with
                $this->acl->rebuild();

                // Pass the current permissions to the view
                $this->view->permissions = $this->acl->getPermissions($role);
            }

            $this->view->role = $role;
        }

        // Pass all the active roles
        $this->view->roles = Roles::find([
            'active = :active:',
            'bind' => [
                'active' => 'Y'
            ]
        ]);
    }
}

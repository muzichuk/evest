<?php
namespace EVEST\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use EVEST\Forms\ChangePasswordForm;
use EVEST\Forms\UsersForm;
use EVEST\Models\Users;
use EVEST\Models\PasswordChanges;
use EVEST\ClientParser\ClientParser;

/**
 * EVEST\Controllers\UsersController
 * CRUD to manage users
 */
class UsersController extends ControllerBase
{

    public function initialize()
    {
        $this->view->setTemplateBefore('private');
    }

    /**
     * Default action, shows the search form
     */
    public function indexAction()
    {
        //$this->persistent->conditions = null;
        //$this->view->form = new UsersForm();
        return $this->dispatcher->forward([
            'action' => 'search'
        ]);
        
    }

    /**
     * Searches for users
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'EVEST\Models\Users', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }
        $parameters['order'] = 'id desc';

        $users = Users::find($parameters);
        if (count($users) == 0) {
            $this->flash->notice("The search did not find any users");
            return $this->dispatcher->forward([
                "action" => "index"
            ]);
        }

        $paginator = new Paginator([
            "data" => $users,
            "limit" => 100,
            "page" => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Creates a User
     */
    public function createAction()
    {
        $form = new UsersForm(null);

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) == false) {
                
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
                
            } else {

                $user = new Users([
                    'first_name' => $this->request->getPost('first_name', 'striptags'),
                    'last_name' => $this->request->getPost('last_name', 'striptags'),
                    'role_id' => $this->request->getPost('role_id', 'int'),
                    'email' => $this->request->getPost('email', 'email')
                ]);

                if (!$user->save()) {
                    $this->flash->error($user->getMessages());
                } else {

                    $this->flash->success("User was created successfully");

                    $form->clear();
                }
            }
        }

        $this->view->form = $form;
    }

    /**
     * Saves the user from the 'edit' action
     */
    public function editAction($id)
    {
        $user = Users::findFirstById($id);
        
        if (!$user) {
            $this->flash->error("User was not found");
            return $this->dispatcher->forward([
                        'action' => 'index'
            ]);
        }

        if ($this->request->isPost()) {

            $user->assign([
                'name' => $this->request->getPost('name', 'striptags'),
                'fullname' => $this->request->getPost('fullname'),
                'rolesId' => $this->request->getPost('rolesId', 'int'),
                'email' => $this->request->getPost('email', 'email'),
                'active' => $this->request->getPost('active')
            ]);

            $form = new UsersForm($user, [
                'edit' => true
            ]);

            if ($form->isValid($this->request->getPost()) == false) {
                
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
                
            } else {

                if (!$user->save()) {
                    $this->flash->error($user->getMessages());
                } else {

                    $this->flash->success("User was updated successfully");

                    $form->clear();
                }
            }
        }

        $this->view->user = $user;

        $this->view->form = new UsersForm($user, [
            'edit' => true
        ]);
    }

    /**
     * Deletes a User
     *
     * @param int $id
     */
    public function deleteAction($id)
    {
        $user = Users::findFirstById($id);
        if (!$user) {
            $this->flash->error("User was not found");
            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if (!$user->delete()) {
            $this->flash->error($user->getMessages());
        } else {
            $this->flash->success("User was deleted");
        }

        return $this->dispatcher->forward([
            'action' => 'index'
        ]);
    }

    /**
     * Users must use this action to change its password
     */
    public function changePasswordAction()
    {
        $form = new ChangePasswordForm();

        if ($this->request->isPost()) {

            if (!$form->isValid($this->request->getPost())) {

                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {

                $user = $this->auth->getUser();

                $user->password = $this->security->hash($this->request->getPost('password'));
                $user->mustChangePassword = 'N';

                $passwordChange             = new PasswordChanges();
                $parser                     = new ClientParser();
                $passwordChange->user       = $user;
                $passwordChange->ip_address = $this->request->getClientAddress();
                $passwordChange->user_agent = $this->request->getUserAgent();
                $ua_info                    = $parser->getUA_info($passwordChange->user_agent);
                $passwordChange->client     = $ua_info->client;
                $passwordChange->os         = $ua_info->os;
                $passwordChange->location   = $parser->getLocation($passwordChange->ip_address);

                if (!$passwordChange->save()) {
                    $this->flash->error($passwordChange->getMessages());
                } else {

                    $this->flash->success('Your password was successfully changed');
                    $form->clear();
                }
            }
        }

        $this->view->form = $form;
    }
}

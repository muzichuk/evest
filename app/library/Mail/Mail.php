<?php
namespace Evest\Mail;

use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;
use Mailgun\Mailgun;

/**
 * Evest\Mail\Mail
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{
    private $domain;
    private $api_key;
    private $mg;
    private $from;
    
    function __construct( $domain = null, $api_key = null )
    {
        $this->domain  = $domain  ?? 'mg.evesttech.com';
        $this->api_key = $api_key ?? 'key-a6a689856947c7cdd44c647f05f10652';
        $this->mg = Mailgun::create( $this->api_key );
        $from = '';
        if( $auth = $this->session->get('auth-identity') ) {
            $from = $auth['first_name'][0] . $auth['last_name'];
        } else {
            $from = 'noreply';
        }
        $this->from = $from . '@' . $this->session->get('domain');
    }
    


    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     * @return string
     */
    public function getTemplate($name, $params)
    {
        $parameters = array_merge([
            'publicUrl' => $this->config->application->publicUrl
        ], $params);

        return $this->view->getRender('emailTemplates', $name, $parameters, function ($view) {
            $view->setRenderLevel(View::LEVEL_LAYOUT);
        });

        return $view->getContent();
    }

    /**
     * Sends e-mails via predefined templates
     *
     * @param array $to
     * @param string $subject
     * @param string $name
     * @param array $params
     * @return bool|int
     * @throws Exception
     */
    public function send($to, $subject, $name, $params)
    {
        $template = $this->getTemplate($name, $params);
        
        return $this->mg->messages()->send( $this->domain, [
          'from'    => $this->from,
          'to'      => $to,
          'subject' => $subject,
          'html'    => $template
        ]);

    }
}

<?php
namespace EVEST\ClientParser;

use Phalcon\Mvc\User\Component;
use GeoIp2\Database\Reader;
use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

/**
 * EVEST\ClientParser\ClientParser
 * Parses IP Addresses for City and UserAgent for Device/Browser
 */
class ClientParser extends Component
{
    /**
     * Returns the City of IP
     *
     * @param string $ip
     * @return string
     * @throws Exception
     */
    public function getLocation($ip)
    {
        $reader = new Reader('/var/www/lib/data/GeoLite2-City_20180206/GeoLite2-City.mmdb');
        $record = $reader->city($ip);
        $location = $record->country->name . ', ' . $record->mostSpecificSubdivision->name . ', ' . $record->city->name . ', ' . $record->postal->code;
        return $location;
    }

    /**
     * Returns the UserAgent's Browser
     *
     * @param string $user_agent
     * @return string
     * @throws Exception
     */
    public function getClient($dd)
    {
        $client = $dd->getClient();
        return $client['name'] . ' : ' . $client['version'];
    }

    /**
     * Returns the UserAgent's OS
     *
     * @param string $user_agent
     * @return string
     * @throws Exception
     */
    public function getOS($dd)
    {
        $os = $dd->getOs();
        return $os['name'] . ' : ' . $os['version'];
    }

    /**
     * Parses the UserAgent
     *
     * @param string $user_agent
     * @return string
     * @throws Exception
     */
    public function parseUA($user_agent)
    {
        DeviceParserAbstract::setVersionTruncation(DeviceParserAbstract::VERSION_TRUNCATION_NONE);
        $dd = new DeviceDetector($user_agent);
        $dd->discardBotInformation();
        $dd->parse();
        
        return $dd;
    }

    /**
     * Returns Human Readable Data From UserAgent
     *
     * @param string $user_agent
     * @return string
     * @throws Exception
     */
    public function getUA_info($user_agent)
    {
        
        $dd = $this->parseUA($user_agent);
        $return = (object)[ 'client' => '', 'os' => '' ];
        
        $return->client = $this->getClient($dd);
        $return->os = $this->getOS($dd);
        
        return $return;
    }
}

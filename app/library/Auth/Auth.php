<?php
namespace EVEST\Auth;

use Phalcon\Mvc\User\Component;
use EVEST\Models\Users;
use EVEST\Models\RememberTokens;
use EVEST\Models\SuccessLogins;
use EVEST\Models\FailedLogins;
use EVEST\ClientParser\ClientParser;

/**
 * EVEST\Auth\Auth
 * Manages Authentication/Identity Management in EVEST
 */
class Auth extends Component
{
    /**
     * Checks the user credentials
     *
     * @param array $credentials
     * @return boolean
     * @throws Exception
     */
    public function check($credentials)
    {

        // Check if the user exist
        $user = Users::findFirst( ['conditions' => 'email = :email:', 'bind' => [ 'email' => $credentials['email'] ] ] );
        if ($user == false) {
            $this->registerUserThrottling(0);
            throw new Exception('Wrong email/password combination');
        }

        // Check the password
        if (!$this->security->checkHash($credentials['password'], $user->password)) {
            $this->registerUserThrottling($user->id);
            throw new Exception('Wrong email/password combination');
        }

        // Check if the user was flagged
        $this->checkUserFlags($user);

        // Register the successful login
        $this->saveSuccessLogin($user);

        // Check if the remember me was selected
        if (isset($credentials['remember'])) {
            $this->createRememberEnvironment($user);
        }

        $this->session->set('auth-identity', [
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'name' => $user->first_name . ' ' . $user->last_name,
            'role' => $user->role->name
        ]);
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param \EVEST\Models\Users $user
     * @throws Exception
     */
    public function saveSuccessLogin($user)
    {
        $parser = new ClientParser();
        $parsed = $parser->parseUA($successLogin->user_agent);
        
        $successLogin             = new SuccessLogins();
        $successLogin->user_id    = $user->id;
        $successLogin->ip_address = $this->request->getClientAddress();
        $successLogin->user_agent = $this->request->getUserAgent();
        $ua_info                  = $parser->getUA_info($successLogin->user_agent);
        $successLogin->client     = $ua_info->client;
        $successLogin->os         = $ua_info->os;
        $successLogin->location   = $parser->getLocation($successLogin->ip_address);
        
        if (!$successLogin->save()) {
            $messages = $successLogin->getMessages();
            throw new Exception($messages[0]);
        }
    }

    /**
     * Implements login throttling
     * Reduces the effectiveness of brute force attacks
     *
     * @param int $user_id
     */
    public function registerUserThrottling($user_id)
    {
        $failedLogin = new FailedLogins();
        $failedLogin->user_id = $user_id;
        $failedLogin->ip_address = $this->request->getClientAddress();
        $failedLogin->user_agent = $this->request->getUserAgent();
        $failedLogin->attempted = time();
        $failedLogin->save();

        $attempts = FailedLogins::count([
            'ip_address = ?0 AND attempted >= ?1',
            'bind' => [
                $this->request->getClientAddress(),
                time() - 3600 * 6
            ]
        ]);

        switch ($attempts) {
            case 1:
            case 2:
                // no delay
                break;
            case 3:
            case 4:
                sleep(2);
                break;
            default:
                sleep(4);
                break;
        }
    }

    /**
     * Creates the remember me environment settings the related cookies and generating tokens
     *
     * @param \EVEST\Models\Users $user
     */
    public function createRememberEnvironment(Users $user)
    {
        $user_agent = $this->request->getUserAgent();
        $token = md5($user->email . $user->password . $user_agent);

        $remember = new RememberTokens();
        $remember->user_id = $user->id;
        $remember->token = $token;
        $remember->user_agent = $user_agent;

        if ($remember->save() != false) {
            $expire = time() + 86400 * 8;
            $this->cookies->set('RMU', $user->id, $expire);
            $this->cookies->set('RMT', $token, $expire);
        } else {
            $messages = $remember->getMessages();
            throw new Exception($messages[0]);
        }
    }

    /**
     * Check if the session has a remember me cookie
     *
     * @return boolean
     */
    public function hasRememberMe()
    {
        return $this->cookies->has('RMU');
    }

    /**
     * Logs on using the information in the cookies
     *
     * @return \Phalcon\Http\Response
     */
    public function loginWithRememberMe()
    {
        $user_id = $this->cookies->get('RMU')->getValue();
        $cookieToken = $this->cookies->get('RMT')->getValue();

        $user = Users::findFirstById($user_id);
        if ($user) {

            $user_agent = $this->request->getUserAgent();
            $token = md5($user->email . $user->password . $user_agent);

            if ($cookieToken == $token) {

                $remember = RememberTokens::findFirst([
                    'conditions' => 'user_id = :user_id: AND token = :token:',
                    'bind' => [
                        'user_id' => $user->id,
                        'token' => $token
                    ]
                ]);
                if ($remember) {

                    // Check if the cookie has not expired
                    if ((time() - (86400 * 8)) < $remember->created_at) {

                        // Check if the user was flagged
                        $this->checkUserFlags($user);

                        // Register identity
                        $this->session->set('auth-identity', [
                            'id' => $user->id,
                            'first_name' => $user->first_name,
                            'last_name' => $user->last_name,
                            'name' => $user->first_name . ' ' . $user->last_name,
                            'role' => $user->role->name
                        ]);

                        // Register the successful login
                        $this->saveSuccessLogin($user);

                        return $this->response->redirect('users');
                    }
                }
            }
        }

        $this->cookies->get('RMU')->delete();
        $this->cookies->get('RMT')->delete();

        return $this->response->redirect('session/login');
    }

    /**
     * Checks if the user is banned/inactive/suspended
     *
     * @param \EVEST\Models\Users $user
     * @throws Exception
     */
    public function checkUserFlags(Users $user)
    {
        if ($user->active != 'Y') {
            throw new Exception('The user is inactive');
        }

    }

    /**
     * Returns the current identity
     *
     * @return array
     */
    public function getIdentity()
    {
        return $this->session->get('auth-identity');
    }

    /**
     * Returns the current identity
     *
     * @return string
     */
    public function getName()
    {
        $identity = $this->session->get('auth-identity');
        return $identity['name'];
    }

    /**
     * Removes the user identity information from session
     */
    public function remove()
    {
        if ($this->cookies->has('RMU')) {
            $this->cookies->get('RMU')->delete();
        }
        if ($this->cookies->has('RMT')) {
            $token = $this->cookies->get('RMT')->getValue();

            $user_id = $this->findFirstByToken($token);
            if ($user_id) {
                $this->deleteToken($user_id);
            }
            
            $this->cookies->get('RMT')->delete();
        }

        $this->session->remove('auth-identity');
    }

    /**
     * Auths the user by his/her id
     *
     * @param int $id
     * @throws Exception
     */
    public function authUserById($id)
    {
        $user = Users::findFirstById($id);
        if ($user == false) {
            throw new Exception('The user does not exist');
        }

        $this->checkUserFlags($user);

        $this->session->set('auth-identity', [
            'id' => $user->id,
            'name' => $user->name,
            'role' => $user->role->name
        ]);
    }

    /**
     * Get the entity related to user in the active identity
     *
     * @return \EVEST\Models\Users
     * @throws Exception
     */
    public function getUser()
    {
        $identity = $this->session->get('auth-identity');
        if (isset($identity['id'])) {

            $user = Users::findFirstById($identity['id']);
            if ($user == false) {
                throw new Exception('The user does not exist');
            }

            return $user;
        }

        return false;
    }
    
    /**
     * Returns the current token user
     *
     * @param string $token
     * @return boolean
     */
    public function findFirstByToken($token)
    {
        $userToken = RememberTokens::findFirst([
            'conditions' => 'token = :token:',
            'bind'       => [
                'token' => $token,
            ],
        ]);
        
        $user_id = ($userToken) ? $userToken->user_id : false; 
        return $user_id;
    }

    /**
     * Delete the current user token in session
     */
    public function deleteToken($user_id) 
    {
        $user = RememberTokens::find([
            'conditions' => 'user_id = :user_id:',
            'bind'       => [
                'user_id' => $user_id
            ]
        ]);

        if ($user) {
            $user->delete();
        }
    }
}

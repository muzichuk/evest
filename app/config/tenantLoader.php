<?php

use EVEST\Models\Tenants;

// find and set tenant_id
$domain = $_SERVER['HTTP_HOST'];
$session = $di->getSession();

if( 1 || $session->get( 'domain' ) !== $domain ) {
    
    $tenant = Tenants::findFirst( "domain = '$domain' AND active = 'Y'" );

    if( $tenant ) {
        $session->set( 'tid', $tenant->id );
        $session->set( 'tenant_name', $tenant->name );
        $session->set( 'domain', $domain );
        $session->set( 'primary_color', 'blue' );
        $session->set( 'secondary_color', 'red' );
    } else {
        // TODO
        // ??? redirect to realtyevest?
    }

}

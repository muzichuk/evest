<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
    'EVEST\Models'      => $config->application->modelsDir,
    'EVEST\Controllers' => $config->application->controllersDir,
    'EVEST\Forms'       => $config->application->formsDir,
    'EVEST'             => $config->application->libraryDir
]);

$loader->register();

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';
require_once APP_PATH . '/models/TenantBase.php';

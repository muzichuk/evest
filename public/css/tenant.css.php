<?php
/*** set the content type header ***/
header("Content-type: text/css");

/*** load session data ***/
use Phalcon\DI\FactoryDefault;
use Phalcon\Session\Adapter\Files as SessionAdapter;

$di = new FactoryDefault();

$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();
    return $session;
});

$session = $di->getSession();



/*
 *
 * CSS Variables
 *
 */
$primary   = $session->get('primary_color')   ?? 'green';
$secondary = $session->get('secondary_color') ?? 'white';

?>


/*
body {
    background-color: <?= $primary ?> !important;
}
*/